import sys
from PyQt6.QtWidgets import QApplication, QFileDialog
from model import BarcodeModel
from view import BarcodeWindow

class BarcodeController:
	def __init__(self):
		self.model = BarcodeModel()
		self.view = BarcodeWindow(self)
		self.update_folder_name()

	def run(self):
		self.view.show()

	def update_folder_name(self):
		directory_suffix = self.model.get_timestamp()
		self.view.set_directory(directory_suffix)

	def on_submit(self):
		if not self.view.get_path():
			self.view.path_input.setStyleSheet("QLineEdit { background-color: #FF7A5E; }")
		else:
			self.model.make_directory(self.view.get_path(), self.view.get_directory())
			self.model.update_data('P', self.view.get_customer_part_number())
			self.model.update_data('1P', self.view.get_supplier_part_number())
			self.model.update_data('Q', self.view.get_quantity())
			self.model.update_data('10D', self.view.get_date_code())
			self.model.update_data('1T', self.view.get_lot_code())
			self.model.update_data('4L', self.view.get_country_of_origin())
			self.model.update_config('btype', self.view.get_barcode_type())
			self.model.update_config('image', self.view.get_2D_barcode())
			self.model.generate_barcode()
			self.update_folder_name()

	def browse_folder(self):
		directory = QFileDialog.getExistingDirectory(self.view, "Select Folder")
		if directory:
			self.view.path_input.setText(directory)
			self.view.path_input.setStyleSheet("")

if __name__ == "__main__":
	app = QApplication(sys.argv)
	controller = BarcodeController()
	controller.run()
	sys.exit(app.exec())
