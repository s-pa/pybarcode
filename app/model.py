import os
import treepoem
from datetime import datetime

class BarcodeModel:
	def __init__(self):
		self.working_dir = ""
		self.data = {
			'P': '',
			'1P': '',
			'Q': '',
			'10D': '',
			'1T': '',
			'4L': ''
		}
		self.config = {
			'btype': '',
			'image': False
		}

	def update_data(self, key, value):
		self.data[key] = value

	def update_config(self, key, value):
		self.config[key] = value

	def generate_barcode(self):
		for key, value in self.data.items():
			if value:
				image = treepoem.generate_barcode(
					barcode_type=self.config['btype'],
					data=f"{key}{value}",
					options={'eclevel': 3, 'showborder': True}
				)
				image.convert("1").save(f"{self.working_dir}/barcode_{key}.png")

	def get_timestamp(self) -> str:
		return datetime.now().strftime("%d%m%Y-%H%M%S")

	def make_directory(self, path, directory):
		full_path = os.path.join(path, directory)
		if not os.path.exists(full_path):
			os.mkdir(full_path)
		self.working_dir = full_path
