from PyQt6.QtGui import QIcon
from PyQt6.QtWidgets import (
	QWidget,
	QLabel,
	QLineEdit,
	QPushButton,
	QComboBox,
	QCheckBox,
	QVBoxLayout,
	QHBoxLayout,
	QGroupBox,
	QFrame
)

class BarcodeWindow(QWidget):
	def __init__(self, controller):
		super().__init__()
		self.controller = controller
		self.init_ui()

	def init_ui(self):
		self.setWindowTitle("PyBarcode")
		#self.setFixedSize(0, 0)
		self.setWindowIcon(QIcon('imgs/icon.png'))

		main_layout = QVBoxLayout(self)

		# Setting Area
		settings_group = QGroupBox("Settings")
		settings_layout = QHBoxLayout()
		
		self.barcode_type_label = QLabel('Barcode type')
		settings_layout.addWidget(self.barcode_type_label)
		
		self.barcode_type_combobox = QComboBox()
		self.barcode_type_combobox.addItems(["pdf417", "qrcode", "azteccode", "code128", "code39"])
		settings_layout.addWidget(self.barcode_type_combobox)
		
		self.barcode_checkbox = QCheckBox("Generate 2D-Barcode")
		self.barcode_checkbox.setChecked(True)
		settings_layout.addWidget(self.barcode_checkbox)
		
		settings_group.setLayout(settings_layout)
		main_layout.addWidget(settings_group)

		# Separator
		separator = QFrame()
		separator.setFrameShape(QFrame.Shape.HLine)
		separator.setFrameShadow(QFrame.Shadow.Sunken)
		main_layout.addWidget(separator)

		# Data area
		data_group = QGroupBox("Data")
		data_layout = QVBoxLayout()
		
		self.customer_part_number_label = QLabel('(P) Customer Part Number')
		self.customer_part_number_input = QLineEdit()
		data_layout.addWidget(self.customer_part_number_label)
		data_layout.addWidget(self.customer_part_number_input)

		self.supplier_part_number_label = QLabel('(1P) Supplier Part Number')
		self.supplier_part_number_input = QLineEdit()
		data_layout.addWidget(self.supplier_part_number_label)
		data_layout.addWidget(self.supplier_part_number_input)

		self.quantity_label = QLabel('(Q) Quantity')
		self.quantity_input = QLineEdit()
		data_layout.addWidget(self.quantity_label)
		data_layout.addWidget(self.quantity_input)

		self.date_code_label = QLabel('(10D) Date Code')
		self.date_code_input = QLineEdit()
		data_layout.addWidget(self.date_code_label)
		data_layout.addWidget(self.date_code_input)

		self.lot_code_label = QLabel('(1T) Lot Code')
		self.lot_code_input = QLineEdit()
		data_layout.addWidget(self.lot_code_label)
		data_layout.addWidget(self.lot_code_input)

		self.country_of_origin_label = QLabel('(4L) Country of Origin')
		self.country_of_origin_input = QLineEdit()
		data_layout.addWidget(self.country_of_origin_label)
		data_layout.addWidget(self.country_of_origin_input)
		
		data_group.setLayout(data_layout)
		main_layout.addWidget(data_group)
  
		# Generate area
		generate_group = QGroupBox()
		generate_layout = QVBoxLayout()

		directory_layout = QHBoxLayout()
		self.directory_label = QLabel('Folder name:')
		self.directory_input = QLineEdit()
		self.directory_input.setReadOnly(True)
		directory_layout.addWidget(self.directory_label)
		directory_layout.addWidget(self.directory_input)
		generate_layout.addLayout(directory_layout)

		path_layout = QHBoxLayout()
		self.path_label = QLabel('Save to:')
		self.path_input = QLineEdit()
		self.path_input.setReadOnly(True)
		path_layout.addWidget(self.path_label)
		path_layout.addWidget(self.path_input)

		self.browse_button = QPushButton('Browse')
		self.browse_button.clicked.connect(self.controller.browse_folder)
		path_layout.addWidget(self.browse_button)
		generate_layout.addLayout(path_layout)

		self.generate_button = QPushButton('Generate')
		self.generate_button.clicked.connect(self.controller.on_submit)
		generate_layout.addWidget(self.generate_button)

		generate_group.setLayout(generate_layout)
		main_layout.addWidget(generate_group)

	def get_barcode_type(self) -> str:
		return self.barcode_type_combobox.currentText()

	def get_2D_barcode(self) -> bool:
		return self.barcode_checkbox.isChecked()

	def get_customer_part_number(self) -> str:
		return self.customer_part_number_input.text()

	def get_supplier_part_number(self) -> str:
		return self.supplier_part_number_input.text()

	def get_quantity(self) -> str:
		return self.quantity_input.text()

	def get_date_code(self) -> str:
		return self.date_code_input.text()

	def get_lot_code(self) -> str:
		return self.lot_code_input.text()

	def get_country_of_origin(self) -> str:
		return self.country_of_origin_input.text()

	def get_directory(self) -> str:
		return self.directory_input.text()

	def set_directory(self, name) -> None:
		self.directory_input.setText(f"barcode_{name}")

	def get_path(self) -> str:
		return self.path_input.text()